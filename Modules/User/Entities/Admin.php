<?php


namespace Modules\User\Entities;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $table = 'admin';

    protected $guard = 'admin';

    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'mobile', 'gender', 'date_of_birth', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
