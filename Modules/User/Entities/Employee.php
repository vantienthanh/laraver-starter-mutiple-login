<?php


namespace Modules\User\Entities;


use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Employee extends Authenticatable
{
    use Notifiable;

    protected $table = 'employee';
    protected $guard = 'employee';

    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'mobile', 'gender', 'date_of_birth', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
