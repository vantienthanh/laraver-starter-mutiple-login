<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Modules\User\Entities\Admin;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::guard($guard)->check()) {
            if ($guard == 'admin') {
                return redirect(route('backend.dashboard'));
            }
            if ($guard == 'employee') {
                return redirect()->intended('/');
            }
            return redirect()->intended('/');
        }

        return $next($request);
    }
}
